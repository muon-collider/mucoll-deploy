# MuColl Deploy

Collection of scripts and pipilines for deploying the Muon Collider Software built with [Spack](https://github.com/spack/spack) and defined in the [mucoll-spack](https://github.com/MuonColliderSoft/mucoll-spack) repository.

## Distribution methods

Muon Collider software stack is distributed in three ways:
* **[CVMFS](https://cernvm.cern.ch/fs/index.html) installation** - optimal for remote use at computing clusters and batch systems with mounted `/cvmfs/` repositories, e.g. on `lxplus9.cern.ch` machines at CERN;
* **[Docker](https://www.docker.com) image** - optimal for running on a local/personal machine, without the need for Internet connection;
* **[Local](https://github.com/MuonColliderSoft/mucoll-spack) installation** - optimal for major code developments and debugging on a personal or virtual machine, giving full control over the installation process. Installation instructions are given in the [mucoll-spack](https://github.com/MuonColliderSoft/mucoll-spack) repository.

> NOTE: Making changes to the compiled code in a **Docker image** is less straightforward than in the case of **CVMFS** or **local installations**. We recommend using it only for running the software, and not for modifying it.

## Deployment targets

To support all the mentioned distribution methods several deployment targets are implemented.
All of them are based on the [`Alma Linux 9`](https://linux.web.cern.ch/almalinux/) distribution, which has a long-term support by CERN till 31.05.2032.

### Stable release on CVMFS

Built using the `mucoll-release` environment configuration with hardcoded versions of all the core packages of the release. All the compiled packages are stored within the corresponding release directory under `/cvmfs/muoncollider.cern.ch/release/mucoll-spack/<release version>`.

### Nightly build on CVMFS

Built using the `mucoll-common` environment configuration for Spack to use the latest version of each package. Individual packages are stored under `/cvmfs/muoncollider.cern.ch/nightly-build/spackages*/`, to allow reusing them as upstream installations during subsequent nightly builds.

Nightly builds are performed every day and the actual release is put under `/cvmfs/muoncollider.cern.ch/nightly-build/mucoll-spack/<date of the build>/`.

### Stable release in a Docker image

Docker image is built using `mucoll-release` environment configuration of a stable release installed on top of the target OS.
The results image is uploaded to the [GitLab Container Registry](https://gitlab.cern.ch/muon-collider/mucoll-deploy/container_registry) of this project.
